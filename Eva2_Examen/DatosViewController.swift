//
//  DatosViewController.swift
//  Eva2_Examen
//
//  Created by TEMPORAL2 on 11/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class DatosViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var tfNombre: UITextField!
    
    @IBOutlet weak var tfCorreo: UITextField!
    
    @IBOutlet weak var tfAsunto: UITextField!
    
    @IBAction func MostrarDatos(sender: AnyObject) {
        let nombre = tfNombre.text
        let correo = tfCorreo.text
        let asunto = tfAsunto.text
        
        let alerta = UIAlertController(title: "Contacto",
            message: "Tu Nombre : \(nombre),\n tu correo: \(correo), \n asunto: \(asunto)", preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Ok", style: .Destructive, handler: nil)
        
        alerta.addAction(action)
        
        presentViewController(alerta, animated: true, completion: nil)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
