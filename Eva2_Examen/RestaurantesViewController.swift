//
//  RestaurantesViewController.swift
//  Eva2_Examen
//
//  Created by TEMPORAL2 on 11/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class RestaurantesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    var restaurantes = ["Dominos", "Carls JR", "Burguer King", "KFC", "Subway", "Las Alitas", "Bob Burguer", "Pizza del rey"]
    var direcciones = ["plaza san angel local 4", "Av. Tecnologico #1212", "libertar #666 col centro", "Plaza Victoria local 5", "Av. Carranza #343" , "Av. Acacia #22", "en algun lugar de un gran pais", "en todos lados"]
    
    
    @IBOutlet weak var lblDireccion: UILabel!
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "celda"
        let celda = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        celda.textLabel?.text = restaurantes[indexPath.row]
        
        return celda
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantes.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let restaurante = restaurantes[indexPath.row]
        let index = indexPath.row
        
        lblDireccion.text = "\(restaurante) \(direcciones[index])"
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
